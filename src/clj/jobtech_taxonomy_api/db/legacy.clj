(ns jobtech-taxonomy-api.db.legacy
  (:require
   [clojure.set :as set]
   [schema.core :as s]
   [datomic.client.api :as d]
   [jobtech-taxonomy-database.nano-id :as nano]
   [jobtech-taxonomy-api.db.database-connection :refer :all]
   [jobtech-taxonomy-api.db.api-util :refer :all]
   [jobtech-taxonomy-api.db.api-util :as api-util]
   [clojure.set :as set]
   [clojure.tools.logging :as log]
   [jobtech-taxonomy-api.routes.parameter-util :as pu]
   [taxonomy :as types]))

(def get-concept-id-from-legacy-id-query
  '[:find ?id
    :in $ ?legacy-id ?type
    :where
    [?c :concept/id ?id]
    [?c :concept/type ?type]
    [?c :concept.external-database.ams-taxonomy-67/id ?legacy-id]])

(def get-concept-id-from-ssyk-2012-query
  '[:find ?id
    :in $ ?ssyk-2012
    :where
    [?c :concept/id ?id]
    [?c :concept/type "ssyk-level-4"]
    [?c  :concept.external-standard/ssyk-code-2012 ?ssyk-2012]])

(def get-concept-id-from-municipality-code-query
  '[:find ?id
    :in $ ?code
    :where
    [?c :concept/id ?id]
    [?c :concept/type "municipality"]
    [?c :concept.external-standard/lau-2-code-2015 ?code]])

(def get-concept-id-from-swedish-region-code-query
  '[:find ?id
    :in $ ?code
    :where
    [?c :concept/id ?id]
    [?c :concept/type "region"]
    [?c  :concept.external-standard/national-nuts-level-3-code-2019 ?code]])

(defn get-concept-id-from-legacy-id [legacy-id type]
  (ffirst (d/q get-concept-id-from-legacy-id-query (get-db) legacy-id type)))

(defn get-concept-id-from-ssyk-2012 [ssyk-2012]
  (ffirst (d/q get-concept-id-from-ssyk-2012-query (get-db) ssyk-2012)))

(defn get-concept-id-from-municipality-code [code]
  (ffirst (d/q get-concept-id-from-municipality-code-query (get-db) code)))

(defn get-concept-id-from-swedish-region-code [code]
  (ffirst (d/q get-concept-id-from-swedish-region-code-query (get-db) code)))

(defn get-concept-id-from-matching-component-id [matching-component-id type]
  (case type
    "ssyk-level-4" (get-concept-id-from-ssyk-2012 matching-component-id)
    "occupation-group" (get-concept-id-from-ssyk-2012 matching-component-id)
    "municipality" (get-concept-id-from-municipality-code matching-component-id)
    "region" (get-concept-id-from-swedish-region-code matching-component-id)
    (get-concept-id-from-legacy-id matching-component-id type)))
